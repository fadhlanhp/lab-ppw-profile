from django.contrib import admin
from .models import Schedule

class ScheduleAdmin(admin.ModelAdmin):
    pass
admin.site.register(Schedule, ScheduleAdmin)

# Register your models here.
