from django.db import models

class Schedule(models.Model):
    name = models.CharField(max_length=27)
    kategori = models.CharField(max_length=15)
    tempat = models.CharField(max_length=30)
    date = models.DateTimeField()

# Create your models here.
