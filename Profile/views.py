from django.shortcuts import render
from .models import Schedule
from .forms import Schedule_Form
from django.http import HttpResponseRedirect
# Create your views here.


def homepage(request):
    return render(request, "home.html")

def register(request):
    return render(request, "register.html")

def schedule_add(request):
    return render(request, "addschedule.html", {'schedule_form' : Schedule_Form})

def confirmation(request):
    return render(request, "confirmation.html")

def education(request):
    return render(request, "education.html")

def schedule_post(request):
    form = Schedule_Form(request.POST)
    if request.method == 'POST' and form.is_valid():
        response = {}
        response['name'] = request.POST['name']
        response['kategori'] = request.POST['kategori']
        response['tempat'] = request.POST['tempat']
        response['date'] = request.POST['date']
        schedule = Schedule(name=response['name'], kategori = response['kategori'],
                          tempat=response['tempat'], date=response['date'])
        schedule.save()
        return render(request, 'confirmation_schedule.html', response)
    else:
        return HttpResponseRedirect('')

def schedule(request):
    schedule = Schedule.objects.all()
    return render(request, 'schedule.html', {'schedule':schedule})


def delete_schedule(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect('/schedule/')

