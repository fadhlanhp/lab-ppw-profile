from django import forms

class Schedule_Form(forms.Form):
    attrsname = {
        'class': 'form-control',
        'placeholder': 'Masukan nama kegiatanmu'
    }

    attrsdate = {
        'class': 'form-control',
        'placeholder': 'Masukan waktu kegiatanmu (Format: YYYY-mm-dd HH:MM:SS)'
    }

    attrstempat = {
        'class': 'form-control',
        'placeholder': 'Masukan tempat dilaksanakannya kegiatanmu'
    }

    listkategori = (
        ('Akademis', 'Akademis'),
        ('Organisasi', 'Organisasi'),
        ('Hiburan', 'Hiburan'),
        ('Lainnya', 'Lainnya'),
    )

    name = forms.CharField(label='Nama Kegiatan', required=True, max_length=40, widget=forms.TextInput(attrs=attrsname))
    date = forms.DateTimeField(label='Waktu Kegiatan', required=True, widget=forms.DateInput(attrs=attrsdate))
    tempat = forms.CharField(label='Tempat Kegiatan', required=True, max_length=30, widget=forms.TextInput(attrs=attrstempat))
    kategori = forms.ChoiceField(label='Kategori', required=True, choices=listkategori, widget=forms.RadioSelect())
    