from django.urls import path
from .views import *

urlpatterns = [
    path('home/', homepage, name='home'),
    path('education/', education, name='education'),
    path('register/', register, name='register'),
    path('register/confirmation/', confirmation, name='confirmation'),
    path('schedule/', schedule, name='schedule'),
    path('schedule/add', schedule_add, name='scheduletambah'),
    path('schedule/done', schedule_post, name='add_schedule'),
    path('schedule/delete', delete_schedule, name='deleteschedule'),    
    path('', homepage, name='')
]
